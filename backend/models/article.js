'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ArticleSchema = Schema({
	name : String,
	nro_serie : String,
	marca : String,
	color : String,
	talla : String,
	precio : String,
	genero : String,
	year : Number,
	category : String,
	subcategory : String,
	description : String,
	image_front : String,
	image_back : String,
	image_left : String,
	image_right : String
});

module.exports = mongoose.model('Article', ArticleSchema);