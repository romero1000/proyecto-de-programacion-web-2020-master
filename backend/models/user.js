'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    name : String,
    surname : String,
    genero : String,
    email : String,
    password : String,
    rol : String,
});

module.exports = mongoose.model('User', UserSchema);
// users --> guarda los documentos en la coleccion