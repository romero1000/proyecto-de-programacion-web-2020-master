'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = 3800;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/tienda-online')
        .then(() =>{
            console.log("Conexion a la base de datos establecida con exito... ");
            //Creacion del servidor
            app.listen(port, ()=>{
                console.log("Servidor corriendo correctamente en la url: localhost:3800");
            });
        })
        .catch(err => console.log(err));