'use strict'

let express = require('express');
let ArticleController = require('../controllers/article');

let router = express.Router();
let multipart = require('connect-multiparty');
let multipartMiddleware = multipart({ uploadDir : './uploads'});

router.get('/home-art', ArticleController.home);
router.post('/save-art', ArticleController.saveArticle);
router.put('/update-art/:id', ArticleController.updateArticle);
router.delete('/delete-art/:id', ArticleController.deleteArticle);
router.get('/article/:id?', ArticleController.getArticle);
router.get('/articulo', ArticleController.getArticulos);
router.get('/camisa', ArticleController.getCamisas);
router.get('/traje', ArticleController.getTrajes);
router.get('/calcetin', ArticleController.getCalcetines);
router.get('/modabanio', ArticleController.getModaBanio);
router.get('/pantalones-cortos', ArticleController.getPantalonesCortos);
router.get('/ropainterior', ArticleController.getRopaInterior);
router.get('/vaqueros', ArticleController.getVaqueros);
router.get('/get-image/:image', ArticleController.getImageFile);
router.post('/upload-image-front/:id',multipartMiddleware, ArticleController.uploadImageFront);
router.post('/upload-image-back/:id',multipartMiddleware, ArticleController.uploadImageBack);
router.post('/upload-image-left/:id',multipartMiddleware, ArticleController.uploadImageLeft);
router.post('/upload-image-right/:id',multipartMiddleware, ArticleController.uploadImageRight);
module.exports = router;