'use strict'

var Article = require('../models/article');
var fs = require('fs');
var path = require('path');

var controller = {
	home : function(req, res){
		return res.status(200).send({
			message: "Hola sere la home para articulo"
		});
	},

	saveArticle: function(req, res){
		let article = new Article();
		let params = req.body;
		article.name = params.name;
		article.nro_serie = params.nro_serie;
		article.marca = params.marca;
		article.color = params.color;
		article.talla = params.talla;
		article.precio = params.precio;
		article.genero = params.genero;
		article.year = params.year;
		article.category = params.category;
		article.subcategory = params.subcategory;
		article.description = params.description;
		article.image_front = null;
		article.image_back = null;
		article.image_left = null;
		article.image_right = null;
		article.save((err, articleStored) =>{//se guarda a mongo
            if(err) return res.status(500).send({message: "Error al guardar el documento."});

            if(!articleStored) return res.status(404).send({message: "No se ha podido guardar el articulo."});

            return res.status(200).send({article : articleStored});
        });
	},

	updateArticle: function(req,res){
        let articleId = req.params.id;
        let update = req.body;
        Article.findByIdAndUpdate(articleId, update,{new:true }, (err, articleUpdated)=>{
            if(err) return res.status(500).send({message: "Error al actualizar"});
            if(!articleUpdated) res.status(404).send({message: "No existe el articulo para actualizarlo"});
            return res.status(200).send({
                article: articleUpdated
            });
        });
    },
    deleteArticle : function(req, res){
        let articleId = req.params.id;
        Article.findByIdAndRemove(articleId, (err, articleRemoved)=>{
            if(err) return res.status(500).send({message: "No se pudo borrar el articulo"});
            if(!articleRemoved) return res.status(404).send({message: "No se puede borrar ese Articulo"});
            return res.status(200).send({
                article : articleRemoved
            });
        });
    },

	getArticle : function(req, res){
        let articleId = req.params.id;
        if(articleId == null) return res.status(404).send({message: "El articulo no existe."});

        Article.findById(articleId, (err, article)=>{
            if(err) return res.status(500).send({message: "Error al devolver los datos."});
            if(!article) return res.status(404).send({message: "El articulo no existe."});
            
            return res.status(200).send({article});
        });
    },

    getArticulos : function(req, res){
        Article.find().exec((err, article)=>{
            if(err) return res.status(500).send({message : "Error al devolver los articulos"});
            if(!article) return res.status(404).send({message: "No hay articulos para mostrar"});
            return res.status(200).send(article);
        });
    },

    getCamisas : function(req, res){
        Article.find({subcategory : 'Camisas'}).exec((err, camisa)=>{
            if(err) return res.status(500).send({message : "Error al devolver las Camisas"});
            if(!camisa) return res.status(404).send({message: "No hay Camisas para mostrar"});
            return res.status(200).send(camisa);
        });
    },

    getTrajes : function(req, res){
        Article.find({subcategory : 'Trajes'}).exec((err, traje)=>{
            if(err) return res.status(500).send({message : "Error al devolver los trajes"});
            if(!traje) return res.status(404).send({message: "No hay trajes para mostrar"});
            return res.status(200).send(traje);
        });
    },

    getCalcetines : function(req, res){
        Article.find({subcategory : 'Calcetines'}).exec((err, calcetines)=>{
            if(err) return res.status(500).send({message : "Error al devolver los Calcetines"});
            if(!calcetines) return res.status(404).send({message: "No hay Calcetines para mostrar"});
            return res.status(200).send(calcetines);
        });
    },
    getModaBanio : function(req, res){
        Article.find({subcategory : 'Moda banio'}).exec((err, modabanio)=>{
            if(err) return res.status(500).send({message : "Error al devolver los Moda Banios"});
            if(!modabanio) return res.status(404).send({message: "No hay Moda Banios para mostrar"});
            return res.status(200).send(modabanio);
        });
    },
    getPantalonesCortos : function(req, res){
        Article.find({subcategory : 'Pantalones cortos'}).exec((err, pantcortos)=>{
            if(err) return res.status(500).send({message : "Error al devolver los Pantalones cortos"});
            if(!pantcortos) return res.status(404).send({message: "No hay Pantalones cortos para mostrar"});
            return res.status(200).send(pantcortos);
        });
    },
    getRopaInterior : function(req, res){
        Article.find({subcategory : 'Ropa interior'}).exec((err, ropainterior)=>{
            if(err) return res.status(500).send({message : "Error al devolver las Ropas Interiores"});
            if(!ropainterior) return res.status(404).send({message: "No hay Ropa Interior para mostrar"});
            return res.status(200).send(ropainterior);
        });
    },
    getVaqueros : function(req, res){
        Article.find({subcategory : 'Vaqueros'}).exec((err, vaquero)=>{
            if(err) return res.status(500).send({message : "Error al devolver las ropas de Vaqueros"});
            if(!vaquero) return res.status(404).send({message: "No hay ropas de Vaqueros para mostrar"});
            return res.status(200).send(vaquero);
        });
    },

	uploadImageFront: function(req, res){
		let articleId = req.params.id;
		let fileName = 'Imagen no subida...';

		if(req.files){
			let filePath = req.files.image_front.path;
			let fileSplit = filePath.split('/');
			let fileName = fileSplit[1];
			let extSplit = fileName.split('\.');
			let fileExt = extSplit[1];

			if(fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif'){
				Article.findByIdAndUpdate(articleId, {image_front: fileName}, {new:true}, (err, articleUpdated)=>{
					if (err)return res.status(500).send({message: "La imagen no se ha subido"});

					if (!articleUpdated) res.status(404).send({message: "El articulo no existe"});

					return res.status(200).send({
						article: articleUpdated
					});
				});
			}else{
				fs.unlink(filePath, (err)=>{
					return res.status(200).send({message: "La extension no es valida"});
				});
			}
		}else{
			return res.status(200).send({
				message: fileName
			});
		}
	},
	uploadImageBack: function(req, res){
		let articleId = req.params.id;
		let fileName = 'Imagen no subida...';

		if(req.files){
			let filePath = req.files.image_back.path;
			let fileSplit = filePath.split('/');
			let fileName = fileSplit[1];
			let extSplit = fileName.split('\.');
			let fileExt = extSplit[1];

			if(fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif'){
				Article.findByIdAndUpdate(articleId, {image_back: fileName}, {new:true}, (err, articleUpdated)=>{
					if (err)return res.status(500).send({message: "La imagen no se ha subido"});

					if (!articleUpdated) res.status(404).send({message: "El articulo no existe"});

					return res.status(200).send({
						article: articleUpdated
					});
				});
			}else{
				fs.unlink(filePath, (err)=>{
					return res.status(200).send({message: "La extension no es valida"});
				});
			}
		}else{
			return res.status(200).send({
				message: fileName
			});
		}
	},
	uploadImageLeft: function(req, res){
		let articleId = req.params.id;
		let fileName = 'Imagen no subida...';

		if(req.files){
			let filePath = req.files.image_left.path;
			let fileSplit = filePath.split('/');
			let fileName = fileSplit[1];
			let extSplit = fileName.split('\.');
			let fileExt = extSplit[1];

			if(fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif'){
				Article.findByIdAndUpdate(articleId, {image_left: fileName}, {new:true}, (err, articleUpdated)=>{
					if (err)return res.status(500).send({message: "La imagen no se ha subido"});

					if (!articleUpdated) res.status(404).send({message: "El articulo no existe"});

					return res.status(200).send({
						article: articleUpdated
					});
				});
			}else{
				fs.unlink(filePath, (err)=>{
					return res.status(200).send({message: "La extension no es valida"});
				});
			}
		}else{
			return res.status(200).send({
				message: fileName
			});
		}
	},
	uploadImageRight: function(req, res){
		let articleId = req.params.id;
		let fileName = 'Imagen no subida...';

		if(req.files){
			let filePath = req.files.image_right.path;
			let fileSplit = filePath.split('/');
			let fileName = fileSplit[1];
			let extSplit = fileName.split('\.');
			let fileExt = extSplit[1];

			if(fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif'){
				Article.findByIdAndUpdate(articleId, {image_right: fileName}, {new:true}, (err, articleUpdated)=>{
					if (err)return res.status(500).send({message: "La imagen no se ha subido"});

					if (!articleUpdated) res.status(404).send({message: "El articulo no existe"});

					return res.status(200).send({
						article: articleUpdated
					});
				});
			}else{
				fs.unlink(filePath, (err)=>{
					return res.status(200).send({message: "La extension no es valida"});
				});
			}
		}else{
			return res.status(200).send({
				message: fileName
			});
		}
	},

	getImageFile : function(req, res){
		var file = req.params.image;
		let path_file = './uploads/'+file;

		fs.exists(path_file, (exists) =>{
			if(exists){
				return res.sendFile(path.resolve(path_file));
			}else{
				return res.status(200).send({
					message: "No existe la imagen..."
				});
			}
		});
	}
}
module.exports = controller;