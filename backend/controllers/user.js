'use strict'
var User = require('../models/user');

var controller = {
    home : function(req, res){
        return res.status(200).send({
            message: 'Soy la home'
        });
    },

    test: function(req, res){
        return res.status(200).send({
            message : 'Soy el metodo o accion test del controlador project'
        });
    },

    saveUser: function(req, res){
        var user = new User();
        var params = req.body;
        user.name = params.name;
        user.surname = params.surname;
        user.genero = params.genero;
        user.email = params.email;
        user.password = params.password;
        user.rol = params.rol;
        //console.log(user);
        user.save((err, userStored) =>{//se guarda a mongo
            if(err) return res.status(500).send({message: "Error al guardar el documento."});

            if(!userStored) return res.status(404).send({message: "No se ha podido guardar el proyecto."});

            return res.status(200).send({user : userStored});
        });
    },

    getUser : function(req, res){
        var userId = req.params.id;
        if(userId == null) return res.status(404).send({message: "El usuario no existe."});

        User.findById(userId, (err, user)=>{
            if(err) return res.status(500).send({message: "Error al devolver los datos."});
            if(!user) return res.status(404).send({message: "El usuario no existe."});
            
            return res.status(200).send({user});
        });
    },

    getUsers: function(req, res){
        User.find({rol : 'administrador'}).exec((err, users)=>{ //en find se puede condicionar
            if(err) return res.status(500).send({message : "Error al devolver los datos"});
            if(!users) return res.status(404).send({message: "No hay usuarios para mostrar"});
            return res.status(200).send(users);
        });
    },

    updateUser: function(req,res){
        var userId = req.params.id;
        var update = req.body;
        User.findByIdAndUpdate(userId, update,{new:true }, (err, userUpdated)=>{
            if(err) return res.status(500).send({message: "Error al actualizar"});
            if(!userUpdated) res.status(404).send({message: "No existe el usuario para actualizarlo"});
            return res.status(200).send({
                user: userUpdated
            });
        });
    },
    deleteUser : function(req, res){
        let userId = req.params.id;
        User.findByIdAndRemove(userId, (err, userRemoved)=>{
            if(err) return res.status(500).send({message: "No se pudo borrar al usuario"});
            if(!userRemoved) return res.status(404).send({message: "No se puede borrar ese usuario"});
            return res.status(200).send({
                user : userRemoved
            });
        });
    }
};

module.exports = controller;
