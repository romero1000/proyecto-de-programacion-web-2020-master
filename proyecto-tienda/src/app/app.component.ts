import { Component } from '@angular/core';
import { EventById, Events, EventService } from './indexedDB_services/event.service';
import { Location } from "@angular/common";
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 	title = 'proyecto-tienda';
 	count_male = 0;
 	count_female = 0;
  public current :string;
  public route: string;
 	
 	  constructor(
        private eventService: EventService,
        private location: Location,
        private router: Router
    ){
        this.current = "H";
        router.events.subscribe(val => {
          if (location.path() != "") {
            this.route = location.path();
          } else {
            this.route = "home";
          }
        });
    }

    get getCurrent():string{
      return this.current;
    }
    sendEvt(evt){
      localStorage.setItem('prenda', evt);
    }
 	
  	onClickmale(){
      localStorage.setItem('prenda', "Null");
  		this.count_male++;
  		console.log("Cantidad de Click en Hombre "+this.count_male);
      var des = "Click en Hombre "+this.count_male;
      const evt: Events = {
        description: des,
      }
      this.eventService.add(evt);
      this.current = "M";
      //console.log(this.current);
  	}
  	onClickFemale(){
      localStorage.setItem('prenda', "Null");
  		this.count_female++;
  		console.log("Cantidad de Click en Mujer "+this.count_female);
      var des = "Click en Mujer "+this.count_male;
      const evt: Events = {
        description: des,
      }
      this.eventService.add(evt);
      this.current = "F";
      //console.log(this.current);
  	}
    onClickHome(){
      localStorage.setItem('prenda', "Null");
      this.current = "H";
      //console.log(this.current);
    }
}
