export class Article{
	constructor(
		public _id : string,
		public name : string,
		public nro_serie : string,
		public marca : string,
		public color : string,
		public talla : string,
		public precio : string,
		public genero : string,
		public year : number,
		public category : string,
		public subcategory : string,
		public description : string,
		public image_front : string,
		public image_back : string,
		public image_left : string,
		public image_right : string
	){}
}
