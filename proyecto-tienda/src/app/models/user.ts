export class User{
	constructor(
		public name: string,
		public surname: string,
		public genero: string,
		public email: string,
		public password: string,
		public rol: string
		){}
}