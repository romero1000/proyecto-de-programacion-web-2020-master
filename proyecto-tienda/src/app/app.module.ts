import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { routing, appRoutingProviders } from './app.routing';
import { CoreModule } from './core/core.module';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { MaleComponent } from './components/male/male.component';
import { FemaleComponent } from './components/female/female.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { CreateComponent } from './components/create/create.component';


// servcio de indexedDB
import { RegisterUserService } from './indexedDB_services/registeruser.service';
import { LoginUserService } from './indexedDB_services/loginuser.service';
import { EventService } from './indexedDB_services/event.service';
import { DetailComponent } from './components/detail/detail.component';
import { EditComponent } from './components/edit/edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactoComponent,
    MaleComponent,
    FemaleComponent,
    LoginComponent,
    RegisterComponent,
    CreateComponent,
    DetailComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    routing,
    HttpClientModule,
    FormsModule,
    CoreModule
  ],
  providers: [
  	appRoutingProviders,
    RegisterUserService,
    LoginUserService,
    EventService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
