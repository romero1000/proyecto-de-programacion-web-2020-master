import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { MaleComponent } from './components/male/male.component';
import { FemaleComponent } from './components/female/female.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { CreateComponent } from './components/create/create.component';
import { DetailComponent } from './components/detail/detail.component';
import { EditComponent } from './components/edit/edit.component';

const appRoutes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'home', component : HomeComponent},
	{path: 'varon', component : MaleComponent},
	{path: 'mujer', component : FemaleComponent},
	{path: 'contacto', component : ContactoComponent},
	{path: 'crear-art', component : CreateComponent},
	{path: 'login', component : LoginComponent},
	{path: 'register', component : RegisterComponent},
	{path: 'detail/:id', component : DetailComponent},
	{path: 'editar-articulo/:id', component : EditComponent},
	{path: '**', component : HomeComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);