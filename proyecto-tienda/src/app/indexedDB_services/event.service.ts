import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import Dexie from 'dexie';
import { DexieService } from '../core/dexie.service';
import { Event } from '../models/event';

export interface Events{
	description : string;
}

export interface EventById extends Events{
	id: number;
}

@Injectable()
export class EventService{
	table: Dexie.Table<EventById, number>;
	constructor(
		private db : DexieService
		){
		this.table = this.db.table('event');

	}

	getAll(){
		return this.table.toArray();
	}

	add(data){
		return this.table.add(data);
	}


}