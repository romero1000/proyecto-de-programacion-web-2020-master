import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import Dexie from 'dexie';
import { DexieService } from '../core/dexie.service';
import { LoginUser } from '../models/login.user';

export interface User{
	email: string;
	password: string;
}

export interface UserById extends User{
	id: number;
}

@Injectable()
export class LoginUserService{
	table: Dexie.Table<UserById, number>;
	constructor(
		private db : DexieService
		){
		this.table = this.db.table('user');

	}

	getAll(){
		return this.table.toArray();
	}

	add(data){
		return this.table.add(data);
	}


}