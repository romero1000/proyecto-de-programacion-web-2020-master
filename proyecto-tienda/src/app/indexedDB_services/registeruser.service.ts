import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import Dexie from 'dexie';
import { DexieService } from '../core/dexie.service';
import { User } from '../models/user';

export interface UserR{
	firstName: string;
	lastName: string;
	genero: string;
	email: string;
	password: string;
	rol: string;
}

export interface UserById extends UserR{
	id: number;
}

@Injectable()
export class RegisterUserService{
	table: Dexie.Table<UserById, number>;
	constructor(
		private db : DexieService
		){
		this.table = this.db.table('user');

	}

	getAll(){
		return this.table.toArray();
	}

	add(data){
		return this.table.add(data);
	}


}