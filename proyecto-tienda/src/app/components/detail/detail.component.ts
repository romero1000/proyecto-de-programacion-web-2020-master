import { Component, OnInit, Input, ViewChild, TemplateRef} from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { Global } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
  providers: [ArticleService]
})
export class DetailComponent implements OnInit {
	@Input() name;
	public url: string;
	public article: Article;
  	constructor(
  		private _articleService: ArticleService,
  		private _router: Router,
  		private _route: ActivatedRoute,
  	) { 
  		//this._modalService.open(NgbActiveModal);
  		this.url = Global.url;
  	}

  	ngOnInit() {
  		this._route.params.subscribe(params => {
  			let id = params.id;
  			this.getArticle(id);
  		})
  	}

  	getArticle(id){
  		console.log("Se ve");
  		this._articleService.getArticle(id).subscribe(
  			response => {
  				this.article = response.article;
  			},
  			error => {
  				console.log(<any>error);
  			}
  		)
  	}

  	deleteArticle(id){
  		this._articleService.deleteArticle(id).subscribe(
  			response => {
  				if(response.article){
  					this._router.navigate(['/varon']);
  				}
  			},
  			error => {
  				console.log(<any>error);
  			}
  		)
  	}


}
