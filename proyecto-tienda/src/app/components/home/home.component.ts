import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { Global } from '../../services/global';
import { AppComponent } from '../../app.component';
import { Location } from "@angular/common";
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ArticleService]
})
export class HomeComponent implements OnInit {
  	public articles : Article[];
  	public url : string;
  	public route: string;
  	public dato: string;
  	//@Input() valor : string;
	constructor(
		private _articleService : ArticleService,
		private location: Location,
		private router: Router
	){ 
		this.url = Global.url;
		router.events.subscribe(val => {
	      if (location.path() != "") {
	        this.route = location.path();
	      } else {
	        this.route = "home";
	      }
	    });
	    //console.log(this.valor);
	}

	ngOnInit() {
		this.dato = localStorage.getItem('prenda');
		console.log(this.dato);
		if(this.dato == "Camisas"){
			this.getCamisas();
		}else if(this.dato == "Moda banio"){
			this.getModaBanio();
		}else if(this.dato == "Trajes"){
			this.getTraje();
		}else if(this.dato == "Calcetines"){
			this.getCalcetines();
		}else if(this.dato == "Pantalones cortos"){
			this.getPantalonesCortos();
		}else if(this.dato == "Ropa interior"){
			this.getRopaInterior();
		}else if(this.dato == "Vaqueros"){
			this.getVaqueros();
		}else{
			this.getArticles();
		}
	}

	getArticles(){
		this._articleService.getArticles().subscribe(
			response =>{
				console.log(response);
				if (response) {
					this.articles = response;
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getCamisas(){
		this._articleService.getCamisas().subscribe(
			response =>{
				//console.log(response);
				if (response) {
					this.articles = response;
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getModaBanio(){
		this._articleService.getModaBanio().subscribe(
			response =>{
				//console.log(response);
				if (response) {
					this.articles = response;
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getTraje(){
		this._articleService.getTraje().subscribe(
			response =>{
				//console.log(response);
				if (response) {
					this.articles = response;
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getCalcetines(){
		this._articleService.getCalcetines().subscribe(
			response =>{
				//console.log(response);
				if (response) {
					this.articles = response;
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getPantalonesCortos(){
		this._articleService.getPantalonesCortos().subscribe(
			response =>{
				//console.log(response);
				if (response) {
					this.articles = response;
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getRopaInterior(){
		this._articleService.getRopaInterior().subscribe(
			response =>{
				//console.log(response);
				if (response) {
					this.articles = response;
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getVaqueros(){
		this._articleService.getVaqueros().subscribe(
			response =>{
				//console.log(response);
				if (response) {
					this.articles = response;
				}
			},
			error =>{
				console.log(error)
			}
		);
	}

	onClick(nombre){
		console.log("se dio click en "+nombre);
	}
}
