import { Component, OnInit } from '@angular/core';
import { LoginUser } from '../../models/login.user';
import { UserById, User, LoginUserService } from '../../indexedDB_services/loginuser.service';
import { EventById, Events, EventService } from '../../indexedDB_services/event.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	public loginUser: LoginUser;
  	constructor(
      private _userService: LoginUserService,
      private eventService: EventService
      ) { 
  		this.loginUser = new LoginUser('','');
  	}

  	ngOnInit() {
  	}

  	onSubmit(form){
  		console.log("Evento submit del login lanzado!!");
      var des = "Evento Submit de login";
      var evt: Events = {
        description: des,
      }
      this.eventService.add(evt);

  		console.log(this.loginUser);
  		form.reset();
  	}

  	eventForm(id){
  		console.log("Evento del Boton "+id);
      var des = "Evento del Boton "+id;
      var evt: Events = {
        description: des,
      }
      this.eventService.add(evt);
  	}

}
