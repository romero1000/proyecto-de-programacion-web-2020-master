import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { UploadService } from '../../services/upload.service';
import { Global } from '../../services/global';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers : [ArticleService, UploadService]
})
export class CreateComponent implements OnInit {
	public title : string;
	public article : Article;
	public save_article;
	public status: string;
	public fileToUpload : Array<File>;
	public fileToUploadTwo : Array<File>;
	public fileToUploadTres : Array<File>;
	public fileToUploadFor : Array<File>;
	public title_Button : string;

	constructor(
		private _articleService : ArticleService,
		private _uploadService : UploadService
	){ 
		this.title = "Crear Articulos";
		this.article = new Article('', '', '', '', '', '', '', '', 2020, '', '', '', '', '', '', '');
		this.status = "";
		this.title_Button = "Insertar Articulo";
	}

	onSubmit(form){
		//console.log(this.article);
		this._articleService.saveArticle(this.article).subscribe(
			response => {
				if (response.article) {
					//subir las imagenes
					this._uploadService.makeFileRequest(Global.url+"upload-image-front/"+response.article._id, [], this.fileToUpload, 'image_front').then((result : any)=>{
						//this.status = "success";
						console.log(result);
						//form.reset();
					});
					this._uploadService.makeFileRequest(Global.url+"upload-image-back/"+response.article._id, [], this.fileToUploadTwo, 'image_back').then((result : any)=>{
						//this.status = "success";
						console.log(result);
						//form.reset();
					});
					this._uploadService.makeFileRequest(Global.url+"upload-image-left/"+response.article._id, [], this.fileToUploadTres, 'image_left').then((result : any)=>{
						//this.status = "success";
						console.log(result);
						//form.reset();
					});
					this._uploadService.makeFileRequest(Global.url+"upload-image-right/"+response.article._id, [], this.fileToUploadFor, 'image_right').then((result : any)=>{
						this.status = "success";
						console.log(result);
						this.save_article = result.article;
						form.reset();
					});

				}else{
					this.status = "failed";
				}
			},
			error =>{
				console.log(<any>error);
			}
		);
	}

	fileChangeEvent(fileInput: any){
		this.fileToUpload =<Array<File>> fileInput.target.files;//todos los archivos que se quiere subir
	}
	fileChangeEventTwo(fileInput2: any){
		this.fileToUploadTwo =<Array<File>> fileInput2.target.files;
	}
	fileChangeEventTres(fileInput3: any){
		this.fileToUploadTres =<Array<File>> fileInput3.target.files;
	}
	fileChangeEventFor(fileInput4: any){
		this.fileToUploadFor =<Array<File>> fileInput4.target.files;
	}

	ngOnInit() {
	}

}
