import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { UserById, UserR, RegisterUserService } from '../../indexedDB_services/registeruser.service';
import { EventById, Events, EventService } from '../../indexedDB_services/event.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[UserService]
})
export class RegisterComponent implements OnInit {
	public registerUser: User;
  	constructor(
		private _userService: UserService,
    private userService: RegisterUserService,
    private eventService: EventService
	){ 
  		this.registerUser = new User('','','','','', '');
  	}

  	ngOnInit() {
  	}

  	onSubmit(form){
      var des = "Evento Submit de Registro";
      var evt: Events = {
        description: des,
      }
      this.eventService.add(evt);

		  console.log(this.registerUser);
      const user: UserR = {
        firstName: this.registerUser.name,
        lastName : this.registerUser.surname,
        genero : this.registerUser.genero,
        email: this.registerUser.email,
        password: this.registerUser.password,
        rol : this.registerUser.rol
      }
      this.userService.add(user);
      

		  this._userService.saveUser(this.registerUser).subscribe(
			  response =>{
				  console.log(response);
			  },
			  error =>{
				  console.log(<any>error);
			  }
		  );
      form.reset();
  	}

  	eventForm(id){
  		console.log("Evento del Boton "+id);
      var des = "Evento del Boton "+id;
      var evt: Events = {
        description: des,
      }
      this.eventService.add(evt);
  	}

}
