import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
import { Global } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-male',
  templateUrl: './male.component.html',
  styleUrls: ['./male.component.css'],
  providers: [ArticleService]
})
export class MaleComponent implements OnInit {
 	public articles : Article[];
 	public articles_Male : Article[];
  	public url : string;
  	public articleD : Article;
  	public dato : string;
	constructor(
		private _articleService : ArticleService,
		private _router: Router,
		private _route: ActivatedRoute,
	){ 
		this.url = Global.url;	
	}

	ngOnInit() {
		this.dato = localStorage.getItem('prenda');
		console.log(this.dato);
		if(this.dato == "Camisas"){
			this.getCamisas();
		}else if(this.dato == "Moda banio"){
			this.getModaBanio();
		}else if(this.dato == "Trajes"){
			this.getTraje();
		}else if(this.dato == "Calcetines"){
			this.getCalcetines();
		}else if(this.dato == "Pantalones cortos"){
			this.getPantalonesCortos();
		}else if(this.dato == "Ropa interior"){
			this.getRopaInterior();
		}else if(this.dato == "Vaqueros"){
			this.getVaqueros();
		}else{
			this.getArticles();
		}
	}

	getArticles(){
		this._articleService.getArticles().subscribe(
			response =>{
				console.log(response);
				if (response) {

					this.articles = response;
					let i = 0;
					this.articles_Male = new Array();
					for (let entry of response) {
  						if(entry.genero == "Masculino"){
  							this.articles_Male[i++] = entry;
  							
  						}
					}
					
				}
			},
			error =>{
				console.log(error)
			}
		);
	}

	getCamisas(){
		this._articleService.getCamisas().subscribe(
			response =>{
				console.log(response);
				if (response) {
					this.articles = response;
					let i = 0;
					this.articles_Male = new Array();
					for (let entry of response) {
							if(entry.genero == "Masculino"){
								this.articles_Male[i++] = entry;
							
							}
					}
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getModaBanio(){
		this._articleService.getModaBanio().subscribe(
			response =>{
				console.log(response);
				if (response) {
					this.articles = response;
					let i = 0;
					this.articles_Male = new Array();
					for (let entry of response) {
							if(entry.genero == "Masculino"){
								this.articles_Male[i++] = entry;
							
							}
					}
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getTraje(){
		this._articleService.getTraje().subscribe(
			response =>{
				console.log(response);
				if (response) {
					this.articles = response;
					let i = 0;
					this.articles_Male = new Array();
					for (let entry of response) {
							if(entry.genero == "Masculino"){
								this.articles_Male[i++] = entry;
							
							}
					}
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getCalcetines(){
		this._articleService.getCalcetines().subscribe(
			response =>{
				console.log(response);
				if (response) {
					this.articles = response;
					let i = 0;
					this.articles_Male = new Array();
					for (let entry of response) {
							if(entry.genero == "Masculino"){
								this.articles_Male[i++] = entry;
							
							}
					}
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getPantalonesCortos(){
		this._articleService.getPantalonesCortos().subscribe(
			response =>{
				console.log(response);
				if (response) {
					this.articles = response;
					let i = 0;
					this.articles_Male = new Array();
					for (let entry of response) {
							if(entry.genero == "Masculino"){
								this.articles_Male[i++] = entry;
							
							}
					}
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getRopaInterior(){
		this._articleService.getRopaInterior().subscribe(
			response =>{
				console.log(response);
				if (response) {
					this.articles = response;
					let i = 0;
					this.articles_Male = new Array();
					for (let entry of response) {
							if(entry.genero == "Masculino"){
								this.articles_Male[i++] = entry;
							
							}
					}
				}
			},
			error =>{
				console.log(error)
			}
		);
	}
	getVaqueros(){
		this._articleService.getVaqueros().subscribe(
			response =>{
				console.log(response);
				if (response) {
					this.articles = response;
					let i = 0;
					this.articles_Male = new Array();
					for (let entry of response) {
							if(entry.genero == "Masculino"){
								this.articles_Male[i++] = entry;
							
							}
					}
				}
			},
			error =>{
				console.log(error)
			}
		);
	}

	getArticle(arti){
		let id = arti._id;
		//console.log(id);
		//console.log(articleD);
		this._articleService.getArticle(id).subscribe(
			response => {
				this.articleD = response.article;
				console.log(this.articleD);
			},
			error => {
				console.log(<any>error);
			}
		);

	}

	deleteArticle(id){
		//let id = arti._id;
		this._articleService.deleteArticle(id).subscribe(
			response => {
				if(response.article){
					this._router.navigate(['/home']);
					console.log("Se elimino el articulo con id: "+id);
				}
			},	
			error => {
				console.log(<any>error);
			}
		);
	}

}
