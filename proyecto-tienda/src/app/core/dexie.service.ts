import Dexie from 'dexie';

export class DexieService extends Dexie {
  constructor() {
    super('MyIndexedDB1');
    this.version(1).stores({
        user: '++id',
          event: '++id',
    });
  }
}