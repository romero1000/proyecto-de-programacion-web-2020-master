import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Article } from '../models/article';
import { Global } from './global';

@Injectable()
export class ArticleService{
    public url:string;
    constructor(
        private _http: HttpClient
    ){
        this.url = Global.url;
    }

    testService(){
        return 'Provando el servicio de angular'
    }
    
    saveArticle(article: Article): Observable<any>{
        let params = JSON.stringify(article);
        let headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

        return this._http.post(this.url+'save-art', params, {headers: headers});
    }

    getArticles():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'articulo', {headers: headers});
    }

    getCamisas():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'camisa', {headers: headers});
    }

    getModaBanio():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'modabanio', {headers: headers});
    }

    getTraje():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'traje', {headers: headers});
    }

    getCalcetines():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'calcetin', {headers: headers});
    }

    getPantalonesCortos():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'pantalones-cortos', {headers: headers});
    }

    getRopaInterior():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'ropainterior', {headers: headers});
    }

    getVaqueros():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'vaqueros', {headers: headers});
    }

    getArticle(id):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'article/'+id, {headers: headers});
    }

    deleteArticle(id):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.delete(this.url+'delete-art/'+id, {headers: headers});
    }

    updateArticle(article):Observable<any>{
        let params = JSON.stringify(article);
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.put(this.url+'update-art/'+article._id, params, {headers: headers});
    }
}